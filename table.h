/* table.h - MeowMeow, a stream encoder/decoder */
// Name: Oze Farris <ofarris@hawaii.edu>
// Date 08_02_2022
// File Name: table.h


#ifndef _TABLE_H
#define _TABLE_H

#define ENCODER_INIT { "purr", "purR", "puRr", "puRR", \
		       "pUrr", "pUrR", "pURr", "pURR", \
                       "Purr", "PurR", "PuRr", "PurR", \
		       "PUrr", "PUrR", "PURr", "PURR" }

#endif	/* _TABLE_H */
