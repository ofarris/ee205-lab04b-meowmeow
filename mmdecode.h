/* mmdecode.h - MeowMeow, a stream encoder/decoder */
// Name: Oze Farris <ofarris@hawaii.edu>
// Date 08_02_2022
// File Name: mmdecode.h

#ifndef _MMDECODE_H
#define _MMDECODE_H

#include <stdio.h>

int mm_decode(FILE *src, FILE *dst);

#endif	/* _MMDECODE_H */
