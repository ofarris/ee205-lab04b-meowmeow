/* mmencode.h - MeowMeow, a stream encoder/decoder */
// Name: Oze Farris <ofarris@hawaii.edu>
// Date 08_02_2022
// File Name: mmencode.h

#ifndef _MMENCODE_H
#define _MMENCODE_H

#include <stdio.h>

int mm_encode(FILE *src, FILE *dst);

#endif	/* _MMENCODE_H */
